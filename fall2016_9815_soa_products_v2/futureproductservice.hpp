/***************************************************************************************************************************
Authors: Christopher Thomas and Haoyuan Yu
Baruch MTH 9815: Software Engineering for Finance
file: future.hpp
description: defines Future ProductServices
***************************************************************************************************************************/

#ifndef futureproductservice_hpp
#define futureproductservice_hpp

#include "productservice.hpp"
#include <boost/shared_ptr.hpp>

typedef boost::shared_ptr<Future> FuturePtr;

/**
* Future Product Service to own reference data over a set of future securities.
* Key is the productId string, value is a Future.
*/
class FutureProductService : public Service<string, FuturePtr>
{

public:
	// FutureProductService constructor
	FutureProductService();

	// Destructor
	virtual ~FutureProductService();

	// Return the future data for a particular future product identifier
	FuturePtr GetData(string productId);

	// Add a future to the service (convenience method)
	void Add(const FuturePtr future);

	// Get all Futures
	std::vector<FuturePtr> GetFutures();

private:
	std::map<string, FuturePtr> futureMap; // cache of future products
};

// Overload the << operator to print out a vector of bonds
ostream& operator<<(ostream &output, const std::vector<BondPtr> &bond_vect);

// Overload the << operator to print out a vector of IRSwaps
ostream& operator<<(ostream &output, const std::vector<IRSwapPtr> &irswap_vect);

// Overload the << operator to print out a vector of futures
ostream& operator<<(ostream &output, const std::vector<FuturePtr> &future_vect);

#endif
