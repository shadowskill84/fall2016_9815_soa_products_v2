/***************************************************************************************************************************
Authors: Christopher Thomas and Haoyuan Yu
Baruch MTH 9815: Software Engineering for Finance
file: publish.hpp
description: Use Boost.Interprocess to publish an int between two C++ programs using shared memory
***************************************************************************************************************************/

#ifndef publish_hpp
#define publish_hpp

#include <string>

namespace myInterProcess
{
	//Publish an int for any C++ process using shared memory to read
	void Publish_Int(std::string val_name, int val);

	//Read a previously published int (throw -1 if invalid named int)
	int Read_Int(std::string val_name);

	//Remove a previously published int from shared memory
	void Remove_Int(std::string val_name);

	//Check if such a named int exists
	bool Check_int(std::string val_name);

	//Remove shared memory
	void Flush_Shared_Memory();

}	//namespace myInterProcess

#endif
