/***************************************************************************************************************************
Authors: Christopher Thomas and Haoyuan Yu
Baruch MTH 9815: Software Engineering for Finance
file: publish.cpp
description: Use Boost.Interprocess to publish an int between two C++ programs using shared memory
***************************************************************************************************************************/

#include "publish.hpp"
#include <boost/interprocess/managed_shared_memory.hpp>

namespace myInterProcess
{
	//constant shared memory name
	const std::string sharedMemName("fall2016_9815_shared_memory_01");

	//constant shared memory size
	const int sharedMemSize(65536);
	
	//Publish an int for any C++ process using shared memory to read
	void Publish_Int(std::string val_name, int val)
	{
		using namespace boost::interprocess;

		//Construct managed shared memory (open if already created)
		managed_shared_memory segment(open_or_create, sharedMemName.c_str(), sharedMemSize);
		
		//Create int initialized to input val
		int *instance = segment.construct<int> (val_name.c_str()) (val);
	}

	//Read a previously published int (throw -1 if invalid named int)
	int Read_Int(std::string val_name)
	{
		using namespace boost::interprocess;

		//open managed shared memory
		managed_shared_memory segment(open_only, sharedMemName.c_str());

		/*Tries to find previously created object. Returns a pointer to the int and the
		**count (1). If not present, the returned pointer is 0*/
		std::pair<int*, std::size_t> result = segment.find<int>(val_name.c_str());

		//throw -1 if invalid named int
		if (result.first == 0) throw -1;

		return *(result.first);
	}

	//Remove a previously published int from shared memory
	void Remove_Int(std::string val_name)
	{
		using namespace boost::interprocess;

		//open managed shared memory
		managed_shared_memory segment(open_only, sharedMemName.c_str());

		//Delete int from shared memory
		segment.destroy<int>(val_name.c_str());
	}

	//Check if such a named int exists
	bool Check_int(std::string val_name)
	{
		using namespace boost::interprocess;

		//open managed shared memory
		managed_shared_memory segment(open_only, sharedMemName.c_str());

		/*Tries to find previously created object. Returns a pointer to the int and the
		**count (1). If not present, the returned pointer is 0*/
		std::pair<int*, std::size_t> result = segment.find<int>(val_name.c_str());

		//if returned pointer is 0 then not found; otherwise int was found
		bool ret_bool(segment.find<int>(val_name.c_str()).first != 0);

		return ret_bool;
	}

	//Remove shared memory
	void Flush_Shared_Memory()
	{
		boost::interprocess::shared_memory_object::remove(sharedMemName.c_str());
	}

}	//namepsace myInterProcess
