/***************************************************************************************************************************
Authors: Christopher Thomas and Haoyuan Yu
Baruch MTH 9815: Software Engineering for Finance
file: test.cpp
description: SOA & the ProductService: Homework

EXERCISE 1
Write a program to publish an int between two C++ programs using shared memory. Google how to use Boost.Interprocess to do this!
implemented in publish.hpp and publish.cpp

EXERCISE 2
Write a Future class in the same way we have a Bond and IRSwap class. 
Also write a FutureProductService like a BondProductService and IRSwapProductService with an example test program to retrieve three futures.
Also add EuroDollarFuture and BondFuture as subclasses of Future.
implemented in future.hpp, future.cpp, futureproductservice.cpp, and futureproductservice.hpp

EXERCISE 3
Write utility methods on the BondProductService and IRSwapProductService to search for all instances of a Bond or IRSwap for a particular attribute.
implemented in productservice.hpp and productservice.cpp
***************************************************************************************************************************/

#include <iostream>
#include "publish.hpp"
#include "products.hpp"
#include "productservice.hpp"
//#include <cstdlib> //std::system (usually included by <iostream>)

int main(int argc, char *argv[])
{
	
	//namespace shorthand
	namespace mip = myInterProcess;
	
	if (argc == 1)
	{	//Parent Process

		//Exercise 1 (continued in child process)***************************************************************************************************************************
		//based on sample code provided here: http://www.boost.org/doc/libs/1_61_0/doc/html/interprocess/quick_guide.html
		
		std::cout << "***Exercise 1 : Publish int between 2 C++ processes***" << std::endl;
		
		//instruct user to enter value to publish
		std::cout << "Please enter int value to publish: ";
		int val;
		std::cin >> val;

		//name to assign to published int
		const std::string val_name("publishIntTest1");

		//publish int with assgined name
		std::cout << "\nPublishing Int Value = " << val << "..." << std::endl;
		mip::Publish_Int(val_name, val);

		//confirm publishing
		if (mip::Check_int(val_name))			
			std::cout << "Int publishing successful!" << std::endl;	//success
		else			
			std::cout << "Int publishing failure! =(" << std::endl;	//failure

		//start child process to read the published int using name passed as param
		std::cout << "Initiating child process..." << std::endl;
		const std::string child_param(std::string(argv[0]) + " " + val_name);
		std::system(child_param.c_str());

		//after test, remove published int from shared memory
		std::cout << "Removing published int..." << std::endl;
		mip::Remove_Int(val_name);

		//confirm removal
		std::cout << ((mip::Check_int(val_name)) ? ("It appears I've failed to do my job =("):("Int removal a success!")) << std::endl;

		//remove shared memory
		mip::Flush_Shared_Memory();
		//end of Exercise 1***************************************************************************************************************************************************
		//********************************************************************************************************************************************************************
		//********************************************************************************************************************************************************************
		//Exercise 2**********************************************************************************************************************************************************
		std::cout << "\n\n***Exercise 2 : Future Product and Service***" << std::endl;

		// Create a FutureProductService (use smart pointers for automatic memory management)
		boost::shared_ptr<FutureProductService> futureProductService(new FutureProductService());

		/*add test futures to future service*/

		FuturePtr future1 = FuturePtr(new Future("ID1", "CL", 5000, 3000, date(2006, Nov, 30)));
		futureProductService->Add(future1);

		FuturePtr future2 = FuturePtr(new Future("ID2", "ES", 5000, 3000, date(2006, Nov, 30)));
		futureProductService->Add(future2);

		FuturePtr future3 = FuturePtr(new Future("ID3", "PL", 5000, 3000, date(2006, Dec, 30)));
		futureProductService->Add(future3);

		FuturePtr future4 = FuturePtr(new EuroDollarFuture("ID4", "EC", 5000, 3000, date(2006, Nov, 16), 1.25));
		futureProductService->Add(future4);

		FuturePtr future5 = FuturePtr(new BondFuture("ID5", "TY", 5000, 3000, date(2006, Nov, 16), Bond("912828M56", CUSIP, "T", 2.25, date(2026, Nov, 16))));
		futureProductService->Add(future5);

		//get all futures from service
		std::vector<FuturePtr> futures = futureProductService->GetFutures();
		std::cout << "\nFutures from Product Service\n" << futures << std::endl;

		//end of Exercise 2***************************************************************************************************************************************************
		//********************************************************************************************************************************************************************
		//********************************************************************************************************************************************************************
		//Exercise 3**********************************************************************************************************************************************************
		std::cout << "\n\n***Exercise 3 : Product Service Utilities***" << std::endl;

		// Create a BondProductService (use smart pointers for automatic memory management)
		boost::shared_ptr<BondProductService> bondProductService(new BondProductService());

		//Create various bonds to add to service
		// Create the 10Y treasury note
		date maturityDate1(2026, Nov, 16);
		string cusip1 = "912828M56";
		BondPtr treasuryBond = BondPtr(new Bond(cusip1, CUSIP, "T", 2.25, maturityDate1));
		bondProductService->Add(treasuryBond);

		// Create the 2Y treasury note
		date maturityDate2(2018, Nov, 5);
		string cusip2 = "912828TW0";
		BondPtr treasuryBond2 = BondPtr(new Bond(cusip2, CUSIP, "T", 0.75, maturityDate2));
		bondProductService->Add(treasuryBond2);

		// Create 30Y mortgage bond
		date maturityDate3(2046, Nov, 16);
		string cusip3 = "912838M56";
		BondPtr mortgageBond = BondPtr(new Bond(cusip3, CUSIP, "M", 4.25, maturityDate3));
		bondProductService->Add(mortgageBond);

		// Create 20Y mortgage bond
		date maturityDate4(2036, Nov, 16);
		string cusip4 = "912828TW2";
		BondPtr mortgageBond2 = BondPtr(new Bond(cusip4, CUSIP, "M", 3.50, maturityDate4));
		bondProductService->Add(mortgageBond2);

		// Create 5Y junk bond
		date maturityDate5(2021, Nov, 16);
		string cusip5 = "932828M56";
		BondPtr junkBond = BondPtr(new Bond(cusip5, CUSIP, "J", 3.25, maturityDate5));
		bondProductService->Add(junkBond);

		// Create 10Y junk bond
		date maturityDate6(2026, Nov, 16);
		string cusip6 = "912829TL0";
		BondPtr junkBond2 = BondPtr(new Bond(cusip6, CUSIP, "J", 5.75, maturityDate6));
		bondProductService->Add(junkBond2);

		//get all "T" bonds
		std::vector<BondPtr> t_bonds = bondProductService->GetBonds("T");
		std::cout << "T bonds\n" << t_bonds << std::endl;

		//get all "J" bonds
		std::vector<BondPtr> j_bonds = bondProductService->GetBonds("J");
		std::cout << "\nJ bonds\n" << j_bonds << std::endl;

		//get all "M" bonds
		std::vector<BondPtr> m_bonds = bondProductService->GetBonds("M");
		std::cout << "\nM bonds\n" << m_bonds << std::endl;

		// Create a IRSwapProductService (use smart pointers for automatic memory management)
		boost::shared_ptr<IRSwapProductService> swapProductService(new IRSwapProductService());

		/*Create various IRswaps to add to service*/

		// Create the Spot 10Y Outright Swap
		date effectiveDate(2016, Nov, 16);
		date terminationDate(2026, Nov, 16);
		string outright10Y = "Spot-Outright-10Y";
		IRSwapPtr outright10YSwap = IRSwapPtr(new IRSwap(outright10Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate, terminationDate, USD, 10, SPOT, OUTRIGHT));
		swapProductService->Add(outright10YSwap);

		// Create the IMM 2Y Outright Swap
		date effectiveDate2(2016, Dec, 20);
		date terminationDate2(2018, Dec, 20);
		string imm2Y = "IMM-Outright-2Y";
		IRSwapPtr imm2YSwap = IRSwapPtr(new IRSwap(imm2Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_3M, effectiveDate2, terminationDate2, USD, 2, IMM, OUTRIGHT));
		swapProductService->Add(imm2YSwap);

		// Create the Forward 20Y Curve Swap
		date effectiveDate3(2016, Dec, 20);
		date terminationDate3(2036, Dec, 20);
		string forward20Y = "FORWARD-Curve-20Y";
		IRSwapPtr forward20YSwap = IRSwapPtr(new IRSwap(forward20Y, THIRTY_THREE_SIXTY, THIRTY_THREE_SIXTY, SEMI_ANNUAL, LIBOR, TENOR_6M, effectiveDate3, terminationDate3, USD, 20, FORWARD, CURVE));
		swapProductService->Add(forward20YSwap);

		//get all IRswaps with specified fixed leg day count convention
		std::vector<IRSwapPtr> swaps = swapProductService->GetSwaps(THIRTY_THREE_SIXTY);
		std::cout << "\nswaps with THIRTY_THREE_SIXTY fixed leg day count convention" << std::endl;
		if (swaps.size() == 0) std::cout << "No IRSwaps with THIRTY_THREE_SIXTY fixed leg day count conventions!" << std::endl;
		else std::cout << swaps << std::endl;

		//get all IRswaps with specified fixed leg pymt frequency
		swaps = swapProductService->GetSwaps(SEMI_ANNUAL);
		std::cout << "\nswaps with SEMI_ANNUAL fixed leg pymt frequency" << std::endl;
		if (swaps.size() == 0) std::cout << "No IRSwaps with SEMI_ANNUAL fixed leg pymt frequency!" << std::endl;
		else std::cout << swaps << std::endl;

		//get all IRswaps with specified floating index
		swaps = swapProductService->GetSwaps(LIBOR);
		std::cout << "\nswaps with LIBOR floating index" << std::endl;
		if (swaps.size() == 0) std::cout << "No IRSwaps with LIBOR floating index!" << std::endl;
		else std::cout << swaps << std::endl;

		//get all IRswaps with a term in years greater than the specified value
		swaps = swapProductService->GetSwapsGreaterThan(5);
		std::cout << "\nswaps with greater than 5 year term" << std::endl;
		if (swaps.size() == 0) std::cout << "No IRSwaps with term greater than 5 years!" << std::endl;
		else std::cout << swaps << std::endl;

		//get all IRswaps with a term in years less than the specified value
		swaps = swapProductService->GetSwapsLessThan(5);
		std::cout << "\nswaps with less than 5 year term" << std::endl;
		if (swaps.size() == 0) std::cout << "No IRSwaps with term less than 5 years!" << std::endl;
		else std::cout << swaps << std::endl;

		//get all IRswaps with a term in years equal to the specified value
		swaps = swapProductService->GetSwaps(5);
		std::cout << "\nswaps with equal to 5 year term" << std::endl;
		if (swaps.size() == 0) std::cout << "No IRSwaps with term equal to 5 years!" << std::endl;
		else std::cout << swaps << std::endl;

		//get all IRswaps with specified swap type
		swaps = swapProductService->GetSwaps(SPOT);
		std::cout << "\nswaps with SPOT swap type" << std::endl;
		if (swaps.size() == 0) std::cout << "No IRSwaps with SPOT swap type!" << std::endl;
		else std::cout << swaps << std::endl;

		//get all IRswaps with specified swap leg type
		swaps = swapProductService->GetSwaps(CURVE);
		std::cout << "\nswaps with CURVE swap leg type" << std::endl;
		if (swaps.size() == 0) std::cout << "No IRSwaps with CURVE swap leg type!" << std::endl;
		else std::cout << swaps << std::endl;

		//end of Exercise 3******************************************************************************************************************************************************
	}
	else
	{	/*Child Process (Continuation of Exercise 1)*/

		std::cout << "Child process initiated.\n" << std::endl;
		
		//get name of published int
		const std::string child_val_name(argv[1]);

		try
		{
			//attempt to read published int
			std::cout << "Published int found!\nInt Value = " << mip::Read_Int(child_val_name) << std::endl;
		}
		catch (int err)
		{
			//unable to read published int
			std::cout << ((err == -1) ? ("Published int not found! I've failed =(") : ("Uknown error!")) << std::endl;
		}
		catch (...)
		{
			//unknown error
			std::cout << "Published int not found. Unknown error!" << std::endl;
		}
	}

	std::cout << std::endl;
	return 0;
}
