/***************************************************************************************************************************
Authors: Christopher Thomas and Haoyuan Yu
Baruch MTH 9815: Software Engineering for Finance
file: future.cpp
description: models Future products for use in a ProductService
***************************************************************************************************************************/

#include "future.hpp"

//default constructor
Future::Future() : Product(0, FUTURE)
{

}

//future constructor
Future::Future(string _productId, string _symRoot, int _initMargin, int _maintMargin, date _lastTradingDay) : Product(_productId, FUTURE)
{
	symRoot = _symRoot;
	initMargin = _initMargin;
	maintMargin = _maintMargin;
	lastTradingDay = _lastTradingDay;

	//construct full symbol: 2 letter root + month code + 2 digit year
	const char month_codes[] = { 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'Q', 'U', 'V', 'X', 'Z' };
	symbol = _symRoot;
	int month = _lastTradingDay.month();
	symbol += month_codes[month-1];
	symbol += to_simple_string(_lastTradingDay).substr(2,2);
}

//destructor
Future::~Future()
{
	//empty
}

//return symbol root
string Future::GetSymRoot() const
{
	return symRoot;
}

//return initial margin
int Future::GetInitMargin() const
{
	return initMargin;
}

//return maintenance margin
int Future::GetMaintMargin() const
{
	return maintMargin;
}

//return last trading day
date Future::GetLastTradingDay() const
{
	return lastTradingDay;
}

//return full symbol
string Future::GetSymbol() const
{
	return symbol;
}

// Overload the << operator to print out the eurodollarfuture
string Future::ToString() const
{
	std::stringstream output;
	output << *this;

	return output.str();
}

// Overload the << operator to print out the future
ostream& operator<<(ostream &output, const Future &future)
{
	output << future.symbol << ": Initial Margin - $" << future.initMargin << ", Maintenance Margin - $" << future.maintMargin << ", Last Trading Day - " << future.lastTradingDay;
	return output;
}

//default constructor
EuroDollarFuture::EuroDollarFuture() : Future()
{

}

//constructor
EuroDollarFuture::EuroDollarFuture(string _productId, string _symRoot, int _initMargin, int _maintMargin, date _lastTradingDay, float _libor3M)
	: Future(_productId, _symRoot, _initMargin, _maintMargin, _lastTradingDay)
{
	libor3M = _libor3M;
}

//return 3 month Libor rate
float EuroDollarFuture::GetLibor3M() const
{
	return libor3M;
}

//destructor
EuroDollarFuture::~EuroDollarFuture()
{
	//empty
}

// Overload the << operator to print out the eurodollarfuture
string EuroDollarFuture::ToString() const
{
	std::stringstream output;
	output << *this << ", Libor 3 Month - " << libor3M << "%";

	return output.str();
}

//default constructor
BondFuture::BondFuture() : Future()
{
	productId = "";
	bondIdType = CUSIP;
	ticker = "";
	coupon = 0;
	maturityDate = date(0, Jan, 0);
}

//constructor
BondFuture::BondFuture(string _productId, string _symRoot, int _initMargin, int _maintMargin, date _lastTradingDay, Bond _bond)
	: Future(_productId, _symRoot, _initMargin, _maintMargin, _lastTradingDay)
{
	productId = _bond.GetProductId();
	bondIdType = _bond.GetBondIdType();
	ticker = _bond.GetTicker();
	coupon = _bond.GetCoupon();
	maturityDate = _bond.GetMaturityDate();
}

//destructor
BondFuture::~BondFuture()
{
	//empty
}

//return underlying bond
Bond BondFuture::GetBond() const
{
	return Bond(productId, bondIdType, ticker, coupon, maturityDate);
}

// Overload the << operator to print out the bondfuture
string BondFuture::ToString() const
{
	std::stringstream output;
	output << *this << ", " << Bond(productId, bondIdType, ticker, coupon, maturityDate);

	return output.str();
}
