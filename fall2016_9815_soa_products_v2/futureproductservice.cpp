/***************************************************************************************************************************
Authors: Christopher Thomas and Haoyuan Yu
Baruch MTH 9815: Software Engineering for Finance
file: future.cpp
description: defines Future ProductServices
***************************************************************************************************************************/

#include "futureproductservice.hpp"

FutureProductService::FutureProductService()
{
	futureMap = map<string, FuturePtr>();
}

FutureProductService::~FutureProductService()
{
	//empty
}

FuturePtr FutureProductService::GetData(string productId)
{
	return futureMap[productId];
}

// Add a future to the service (convenience method)
void FutureProductService::Add(const FuturePtr future)
{
	futureMap.insert(std::pair<string, FuturePtr>(future->GetProductId(), future));
}

//Get all Futures
std::vector<FuturePtr> FutureProductService::GetFutures()
{
	//vector of futures to return
	vector<FuturePtr> futureVect(0);

	//iterate over future map
	for (map<string, FuturePtr>::iterator it = futureMap.begin(); it != futureMap.end(); ++it)
		futureVect.push_back(it->second);

	//return vector of futures now containing all futures
	return futureVect;
}

// Overload the << operator to print out a vector of bonds
ostream& operator<<(ostream &output, const std::vector<BondPtr> &bond_vect)
{
	for (size_t i = 0; i < bond_vect.size(); ++i)
		output << bond_vect[i]->ToString() << ((i == bond_vect.size() - 1) ? ("") : ("\n"));

	return output;
}

// Overload the << operator to print out a vector of IRSwaps
ostream& operator<<(ostream &output, const std::vector<IRSwapPtr> &irswap_vect)
{
	for (size_t i = 0; i < irswap_vect.size(); ++i)
		output << irswap_vect[i]->ToString() << ((i == irswap_vect.size() - 1) ? ("") : ("\n"));

	return output;
}

// Overload the << operator to print out a vector of futures
ostream& operator<<(ostream &output, const std::vector<FuturePtr> &future_vect)
{
	for (size_t i = 0; i < future_vect.size(); ++i)
		output << future_vect[i]->ToString() << ((i == future_vect.size() - 1) ? ("") : ("\n"));

	return output;
}
