/***************************************************************************************************************************
Authors: Christopher Thomas and Haoyuan Yu
Baruch MTH 9815: Software Engineering for Finance
file: future.hpp
description: models Future products for use in a ProductService
***************************************************************************************************************************/

#ifndef future_hpp
#define future_hpp

#include "products.hpp"

/**
* Modeling of a Future Product
*/

class Future : public Product
{
public:
	//default constructor
	Future();

	//custom constructor
	Future(string _productId, string _symRoot, int _initMargin, int _maintMargin, date _lastTradingDay);

	//destructor
	virtual ~Future();

	//return symbol root
	string GetSymRoot() const;

	//return initial margin
	int GetInitMargin() const;

	//return maintenance margin
	int GetMaintMargin() const;

	//return last trading day
	date GetLastTradingDay() const;

	//return full symbol
	string GetSymbol() const;

	//return string representation of future
	virtual string ToString() const;

	// Overload the << operator to print out the future
	friend ostream& operator<<(ostream &output, const Future &future);

private:
	string symRoot;			//symbol root var
	int initMargin;			//initial margin var
	int maintMargin;		//maintenance margin var
	date lastTradingDay;	//last trading day var
	string symbol;			//full symbol var
};

class EuroDollarFuture : public Future
{
public:
	//default constructor
	EuroDollarFuture();

	//custom constructor
	EuroDollarFuture(string _productId, string _symRoot, int _initMargin, int _maintMargin, date _lastTradingDay, float _libor3M);

	//destructor
	virtual ~EuroDollarFuture();

	//return 3 month Libor rate
	float GetLibor3M() const;

	//return string representation of eurodollarfuture
	virtual string ToString() const;

private:
	float libor3M;	//3 month libor rate
};

class BondFuture : public Future
{
public:
	//default constructor
	BondFuture();

	//custom constructor
	BondFuture(string _productId, string _symRoot, int _initMargin, int _maintMargin, date _lastTradingDay, Bond _bond /*bond that future is based on*/);

	//destructor
	virtual ~BondFuture();

	//return underlying bond
	Bond GetBond() const;

	//return string representation of bondfuture
	virtual string ToString() const;

private:
	string productId;		// underlying bond product identifier variable
	BondIdType bondIdType;	// underlying bond bond id type variable
	string ticker;			// underlying bond ticker variable
	float coupon;			// underlying bond coupon variable
	date maturityDate;		// underlying bond maturity date variable
};


#endif
