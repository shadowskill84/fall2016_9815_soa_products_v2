# README #

### Class ###
* MATH 9815: Software Engineering for Finance
* Homework 02: SOA & the ProductService (Revisited)

### Authors ###
* Thomas,Christopher

### Script Summary ###
Test new implementations of various aspects of Service Oriented Architecture. 

### Usage ###
Program built with Visual Studio Express 2013. To load solution in Visual Studio simply open "fall2016_9815_cpp_hw02_soa_service.sln".  
To run with g++, change to directory with _test.cpp_ and execute the following command line: **g++ -I <boost/path> *.cpp --std=c++11 -pthread -lrt -lboost_date_time**  
You may need to install additional libraries, such as datetime package with **sudo apt-get install libboost-date-time-dev**  

### Libraries needed: ###
* iostream
* string
* map
* boost/shared_ptr.hpp
* boost/date_time/gregorian/gregorian.hpp
* boost/interprocess/managed_shared_memory.hpp

### Details ###
Exercise 1: Publish an int between two C++ programs using shared memory  
Implemented in publish.hpp, publish.cpp

Exercise 2: Write a Future class, a FutureProductService, and add EuroDollarFuture and BondFuture as subclasses of Future  
Test new implementation of ProductService for Futures and its sub-classes BondFuture and EuroDollarFuture  
Implemented in future.hpp, future.cpp, futureproductservice.hpp, futureproductservice.cpp

Exercise 3: Write utility methods for BondProductService and IRSwapProductService to find all instances for a particular attribute  
Implemented in productservice.hpp, productservice.cpp